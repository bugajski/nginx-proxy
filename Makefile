#!make

.PHONY: default
default: start
tail=100

network:
	docker network create nginx_proxy


start:
	docker-compose up -d

stop:
	docker-compose stop

down:
	docker-compose down

ps:
	docker-compose ps

log:
	docker-compose logs --tail=$(tail)


whole:
	$(eval tail := 'all')


clean:
	docker network rm nginx_proxy
	sudo rm -rf certs
	sudo rm -rf html

delete: down clean

restart: stop start

recreate: delete default
